[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH="$PATH:$HOME/bin"
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="brave"

# automatically run startx when loggin in on ttyq
[[ ! $DISPLAY && $XDG_VTNR -eq 1 ]] && exec sartx -- vt1
